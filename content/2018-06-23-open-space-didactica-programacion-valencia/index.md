---
title: "Primer Open Space sobre didáctica de la programación en Valencia"
date: 2018-06-23
summary: "Un Open Space es un evento que se inicia sin una agenda cerrada, en el cual los asistentes asumen la responsabilidad de proponer, votar y desarrollar los contenidos. He tenido la ocasión de asistir a uno que trataba sobre la educación aplicada al desarrollo de software. Sus organizadores le han dado una vuelta de tuerca adicional al formato, y ha resultado ser realmente interesante."
tags: ["eventos"]
categories:
  - eventos
image: "images/valencia.jpg"
---

El pasado sábado tuve la ocasión de asistir a un [evento totalmente diferente](https://ensenyar-a-programar.github.io/open-space-valencia-2018/) a todos los que había ido anteriormente. La temática ya era de por sí original, no tanto por lo exótico del tema, sino porque cuando hablamos de programación, se suele poner mucho el foco en el lado técnico y menos en el humano, que es fundamental en la transmisión de conocimientos. De hecho, fue un evento sin ordenadores ni dispositivos electrónicos. Estoy escribiendo este artículo gracias a las notas que tomé en mi bonito cuaderno analógico.

Pues bien, más allá de la temática, hubo dos cosas muy originales por encima de todo. La primera, el lugar. Nada de *coworkings*, ni sedes de empresas, ni palacios de congresos, ni centros de innovación, nada. En la calle. En Valencia. En el parque. Concretamente, en el cauce del Turia, más o menos a la altura de la estación de autobuses. Unas cuantas sillas, unas esterillas para el suelo y un picnic extraordinario con el que nos recibieron nuestros anfitriones valencianos, en su mayoría organizadores y alumnos de la [Devscola](http://www.devscola.org/). Desde Madrid también llegaron algunos amigos de [Adalab](http://adalab.es/), y por último, unos pocos más que, aunque sin tener un vínculo particular con la educación, queríamos aprender más sobre el tema.

La segunda, el formato. Anteriormente había asistido a *open spaces*, entre ellos a un par de ediciones del [AOS](http://aos2017.agile-spain.org/). Pero estos eventos tienen un formato un poco más rígido, ya que, salvo los temas a tratar, estipulan ciertas cosas antes de empezar, como por ejemplo el tiempo máximo de cada sesión, los espacios o los horarios. Los asistentes son los que dan un *pitch* al inicio explicando de qué quieren hablar, para proceder después a una votación y a la confección del horario en base a los resultados. También se ha puesto este modelo de moda en eventos más clásicos como complemento.

![Temas que salieron en el Open Space](images/valencia-1.jpg#img-left-out "Temas que salieron en el Open Space")

Pero aquí era mucho mas "rudimentario", lo cual lo hacía también muy atractivo. Empezábamos en un círculo central, dos o tres personas proponían un tema, y cada una señalaba "su árbol". Los interesados en cada tema se iban con cada una de ellas bajo su sombra, y tras un buen rato de debate, volvíamos al círculo central y poníamos las ideas principales en común. Según el interés generado, se iban ampliando o reduciendo los tiempos, unificando o divergiendo las temáticas, etc. Y por supuesto, como en todo *open space*, se aplicaba la conocida "[regla de los dos pies](http://reeelab.com/2013/02/16/manual-de-instrucciones-para-un-open-space/)", que es probablemente el rasgo más distintivo de este tipo de dinámicas y que fomenta que cada participante pueda moverse libremente de un debate a otro, estando en todo momento en el lugar donde más pueda aprender o contribuir. El horario aproximado que seguimos al final fue el que se ve en la foto, pero no se pactó inicialmente, sino que lo fuimos construyendo y modificando a medida que avanzaba el día, en función de lo que diera de sí cada tema.

Repetimos este proceso cuatro veces, haciendo una pausa a la mitad para comer. A continuación detallo las principales conclusiones que extraje de los debates a los que asistí.

### Relación entre mentor y mentorizado

- El líder de la relación ha de ser el mentorizado, no el mentor. Es decir, el mentorizado ha de ser el que inicie la relación, tome la iniciativa y marque los ritmos, y además no debe ser presionado.
- El objetivo es llevar al mentorizado a la autosuficiencia y tratarle como tal.
- Si hay dinero de por medio, es probable que el juego de equilibrios, el grado de compromiso y otras variables cambien, con lo cual ya no suele haber un mentor, sino un coach o maestro.
- Al no haber dinero, hay que tener claro que ambos van a aprender y beneficiarse, ha de ser una relación *quid pro quo*.
- Como mentor, es importante expresar tus dudas y autocuestionarte, que no te vean como una especie de ser supremo infalible, que te discutan.
- A la hora de transmitir los valores, hay que hacerlo para que el mentorizado sea capaz de elaborar su propio código ético. El mentor nunca debe tratar de imponer los suyos. De otro modo se puede caer en el lado oscuro del mentor, que es el paternalismo y el adoctrinamiento.
- No nos podemos olvidar tampoco del lado oscuro del mentorizado: la sumisión y la no existencia de pensamiento crítico.

### Los valores de una organización educativa

- Quizá lo que más polémica causó aquí fue el debate de si los valores deben estar formalizados por escrito o si la práctica diaria debe ser la que los establezca de forma implícita. Pero sí que hay que asumir que la mera gestión del día a día ya marca necesariamente unos valores.
- Es importante distinguir entre los valores de la organización y los de cada uno de los miembros individuales: aceptar los valores no es lo mismo que compartirlos.
- Suele ser deseable que los valores perduren, sobre todo si están muy marcados, para reforzar la identidad. Pero al mismo tiempo deben ser adaptables a los tiempos y a las circunstancias, sin tener miedo a introducir matices. La gestión asamblearia y los debates suelen ser eficaces para conseguir esto.

### Experiencias didácticas en remoto

- Lo más importante aquí es que no se debe trasladar una experiencia presencial al remoto tal cual, ya que esto suele fracasar. Hay una infinidad de matices que hay que trabajar.
- Llevamos miles de años de experiencias presenciales y sólo unos pocos en Internet, con lo que el campo de experimentación es enorme, y literalmente acabamos de empezar.
- Las comunidades *online* pueden ser muy satisfactorias. Pero ojo, un foro y unos cuantos vídeos no son una comunidad, hace falta mucho más que eso para ser una verdadera comunidad.

### Aprendizaje desde cero

- A la hora de implementar un plan de estudios, sobre todo dirigido a gente que parte de cero, ¿es mejor dar más libertad y explicar todas las opciones, o esto puede acabar confundiendo y atorando a los estudiantes?
- Estamos demasiado acostumbrados a que alguien con autoridad o con conocimientos nos aporte la solución "buena" como dogma y no se discutan demasiado las cosas.
- *No pain, no game*. Es necesario aprender a gestionar la frustración. También debemos asumir las consecuencias de las malas decisiones y aprender a dar marcha atrás cuando lleguemos a un camino sin salida. No conviene obcecarse.
- Si hay que priorizar una opción, sobre todo en programación, es importante dar importancia a la opción más simple (más mantenible, más comprensible a largo plazo), aunque quizá no al principio, ya que llegar a soluciones sencillas es muchas veces lo más complicado, y es la experiencia la que las suele dar.

![Asistentes en círculo](images/valencia-2.jpg "Asistentes en círculo")

Asistimos en torno a 30 personas. Creo que para ser la primera edición, y para ser algo más o menos experimental salió realmente bien. Quizá para futuras ediciones estaría genial conseguir un grupo más heterogéneo y contar con las experiencias de profesores de programación de la escuela pública, la universidad o de la educación reglada en general, ya que nos centramos mucho en filosofías muy diferentes a esta, y que a día de hoy no son las más habituales.

Y no quiero terminar sin dejar de poner en valor iniciativas como la Devscola, que además de innovar y experimentar continuamente en la didáctica de la programación, se centran en dar oportunidades a la gente a nivel local, trabajando el cara a cara, la mentorización y las relaciones personales, algo muy necesario en un sector cuyas dinámicas internas no siempre van en esta dirección.
