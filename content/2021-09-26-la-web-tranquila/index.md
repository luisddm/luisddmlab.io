---
title: "La web tranquila"
date: 2021-09-26
summary: "La andadura de la web comenzó hace unos treinta años. En sus orígenes era una plataforma muy simple para compartir conocimiento usando texto e hiperenlaces, lo cual permitía al usuario desplazarse de enlace en enlace para descubrir nuevos contenidos. A día de hoy, la web ya no tiene nada que ver con esto. Se ha convertido en un lugar diseñado para capturar y retener la atención y el tiempo de los usuarios lo máximo posible, a cualquier precio. Sin embargo, no todo el mundo se ha rendido a este nuevo paradigma."
tags: ["aa"]
categories:
  - opinión
---

Hay un sitio web, bastante curioso, que visito de vez en cuando. Se llama [electoral-vote.com](http://electoral-vote.com) y lo descubrí hace ya unos cuantos años. Se trata de una web donde aparece un mapa de USA y un comentario diario de actualidad política. Originalmente era un agregador de encuestas electorales, es decir, los autores iban tomando datos demoscópicos de diferentes fuentes, hacían la media y, mostraban de este modo las tendencias de intención de voto en cada estado, con el fin de intentar "predecir" el resultado final de las elecciones. Al principio apenas se incluía texto adicional y el sitio únicamente estaba en marcha cuando se acercaban elecciones, pero de un tiempo a esta parte, se ha convertido en un blog de análisis político.

Sus dos autores escriben a diario. Se trata de:

- [Andrew S. Tanenbaum](https://en.wikipedia.org/wiki/Andrew_S._Tanenbaum) (Votemaster), un famoso profesor de ciencias de la computación, que ha escrito innumerables libros y papers, y que bien puede considerarse como el abuelo de Linux (fue el creador de Minix, que a su vez sirvió como inspiración a Linus Torvalds).
- [Christopher Bates](https://www.electoral-vote.com/evp2015/Pres/Maps/Sep28.html#item-1) (Zenger), un historiador experto en historia norteamericana y profesor universitario residente en California. Aquí podemos verle dando su charla ["The future of the past"](https://www.youtube.com/watch?v=m5_BRLsAZOU).

Sus análisis son extensos, exhaustivos, irónicos y muy disfrutables para los auténticos yonkis de la política, aunque a veces pecan de repetitivos o de exceso de partidismo, sobre todo por parte de Zenger.

Pero, al margen del contenido, que me gustaría dejar en segundo plano, ¿por qué digo que es un sitio web "curioso"? Por varios motivos. Han optado por mantener el sitio prácticamente igual que cuando nació, en 2004, a nivel de funcionalidad. Podría pasar incluso por una web de los 90 y a nadie le extrañaría demasiado.

Por otra parte, los autores no aparecen en TV, ni son streamers, ni nada por el estilo. No son estrellas mediáticas, cuando podrían perfectamente haberse sumado a alguna de las innumerables tertulias políticas o similares si hubieran querido, y haber conseguido bastante dinero y popularidad. Desconozco si tienen redes sociales, y en su caso, cómo las usan, pero en su caso no creo que hagan mucho más que observar.

Otro punto interesante es cómo se gestiona la participación de los lectores en el sitio web. Inicialmente no había ningún tipo de feedback, pero en los últimos tiempos habilitaron dos cuentas de correo, una para preguntas y otra para comentarios de los lectores. Los sábados publican (y responden) preguntas, y los domingos publican comentarios.

Es decir, que pudiendo tener una web con todo tipo de artefactos "modernos" que han ido apareciendo con los años, como toneladas de imágenes, vídeos y Javascript, botones para compartir contenido en redes sociales, botones de "me gusta", rankings de popularidad, formularios incrustados para comentar, etc., no lo han hecho. Han descartado todo eso, y siguen usando el email de toda la vida para interactuar, nada más.

¿Por qué estoy hablando de todo esto? Porque aunque parezca increíble, es un sitio web con muchos lectores, y sobre todo, muy fieles, por lo que se deduce de los comentarios que publican. Y no solo eso, la calidad del debate que se produce de este modo es mucho mayor que en cualquiera de los sitios web que se han ido cargando de características "sociales" con los tiempos. Se puede estar más o menos de acuerdo con la línea editorial (que es más bien próxima a los postulados demócratas), pero en cualquier caso es un sitio que yo calificaría como honesto, por diferentes motivos:

- Cuando un lector quiere participar en el debate, primero tiene que molestarse en ir a su cliente de email y redactar algo mínimamente elaborado. Es decir, se disuade a los que van con el calentón insultando o poniendo exabruptos en los típicos formularios que hay al final de los artículos, que no requieren más que escribir un par de líneas y pulsar un botón.
- Además, el lector-comentarista tiene que esforzarse por escribir algo interesante y que aporte a la comunidad, ya que las cosas irrelevantes u ofensivas no pasan el filtro, que está compuesto por dos personas, y no una supuesta IA entrenada para censurar unas pocas palabras clave y poco más. Esto podría tacharse de "poco democrático" por seguir el modelo de dictador benévolo, y sería un debate interesante. Pero tampoco veo cómo puede ser democrático un sitio web donde todo el mundo opine de todo sin una mínima reflexión, y donde haya barra libre para el insulto.
- Es un sitio de texto puro y duro, que no intenta robar la atención de nadie ni controlar que se permanezca mucho rato en su dominio, más que el justo y necesario para leer los puntos que nos interesen. Tampoco hay clickbaits, ni anuncios invasivos, ni se intenta hacer profiling de los visitantes. Como mucho hay algún entretenido intercambio de impresiones entre los lectores algunos domingos que engancha bastante, pero de una forma bastante más sana de lo habitual.
- Sus autores, además de no usar ningún tipo de truco psicológico, tampoco rinden cuentas a nadie. Se podrá estar a favor o en contra de lo que escriben, pero no actúan bajo presión ni interés de nadie más que de ellos mismos.

Con esto quería ilustrar cómo hemos asumido como normal la web de hoy en día, con todos los trucos posibles para mantenernos pegados, monetizar la atención y dejar nuestros navegadores web extenuados debido al consumo desorbitado de recursos, aunque tengamos el ordenador más moderno del mercado. Es posible hacer otra web y formar una comunidad mucho más sana alrededor de ella, con espacio para el debate pero no para el griterío ni los insultos.

No necesitamos usar algo por el mero hecho de que exista. Si una tecnología más antigua es suficientemente eficiente, nos ayuda a llevar un ritmo de vida más pausado y sostenible, y esto nos satisface, deberíamos poder seguir usándola. No deberíamos vernos obligados a actualizar todo sin más, solamente porque lo tengamos ahí delante, sin pensar en las consecuencias.

En este sentido, la propuesta del protocolo Gemini, que básicamente consiste en limitar la funcionalidad de la web, en un experimento muy interesante, aunque minoritario, en el panorama actual. Gemini intenta recuperar algunas esencias de la web, como la simplicidad, sin renegar de otras, como la seguridad. Obviamente no pretende reemplazar la web, pero su diseño debería ayudarnos a entender cómo era antes la web y qué elementos merece la pena retener. Del mismo modo, deberíamos no abusar de los CGIs en Gemini, para evitar que se convierta en una nueva web, solo que un poco más underground. Puede que con esta filosofía consigamos poner nuestro granito de arena para lograr un internet más sano y constructivo.
